

/**
 * This object contains the data for the hardcoded users so far.
 */
var data = {
    Chase : ["Chase", "cstrickler01@hamline.edu", "320-805-0482" ],
    Ricky : ["Ricky", "rickman@rocketmail.com", "320-877-7564" ],
    Jennifer : ["Jennifer", "someemailaddress@hotmail.com", "850-877-7234" ],
    Lucas: ["Lucas", "lukeeey33@jahsdlfja.kcjoa", "834-323-2645" ]
};
/**
 * This method takes an a string that represents the person we want to
 * display to the user.
 * @param {The string that represents the user in the phone book} name
 */
function getInformation(name) {
    var parent = document.getElementById("out");
    var child = parent.querySelectorAll("p");

    if (data[name] !== null && data[name] !== undefined) {
        var info = data[name];
        for (i = 0; i < info.length; i++) {
            child[i].innerHTML = info[i];
        }
    }
}

/**
 * This method will render a new button to the page to display user information
 * @param {The array containing the user information!} data
 */
function addUser() {
    
    // grab the list we want to append to and create a button!
    var list = document.getElementById("list");
    var listItem = document.createElement("li");
    var button = document.createElement("button");

    var name = document.forms["myForm"]["firstname"].value;
    var email = document.forms["myForm"]["email"].value;
    var phone = document.forms["myForm"]["phone"].value;

    // add the new user to the data object!
    data[name] = [name, email, phone]

    // assign the properties for the button we are adding!
    button.className = "name";
    button.id = name;
    button.addEventListener("click",function() {getInformation(this.id)});
    button.innerHTML = name;

 
    listItem.appendChild(button);
    list.appendChild(listItem);

    return false;
}

/**
 * This method removes a given user in the contact list
 */
function removeUser() {
    var contact = document.getElementById("fname").innerHTML;
    var button = document.getElementById(contact);

    button.remove();
    data[contact] = null;

    // refresh the phonebook so that it is empty after we remove the current user
    reset();

}
/**
 * This method is called to reset the current phonebook page
 */
function reset() {
    var parent = document.getElementById("out");
    var child = parent.querySelectorAll("p");

    for (i = 0; i < child.length; i++) {
        child[i].innerHTML = " ";
    }
}
function myFunction(list){
    var text = "";
    var inputs = document.querySelectorAll("input[type=text]");
    for (var i = 0; i < inputs.length; i++) {
        text += inputs[i].value;
    }
    var li = document.createElement("li");
    var node = document.createTextNode(text);
    li.appendChild(node);
    document.getElementById("list").appendChild(li);
}
